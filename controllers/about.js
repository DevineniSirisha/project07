const express = require('express')
const api = express.Router()

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------


// GET t7
api.get('/t7', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t7/index.ejs',
        { title: 'Team 7', layout: 'layout.ejs' })
})
api.get('/t7/a', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t7/a/index.ejs',
        { title: 'Sai Sirisha Devineni', layout: 'layout.ejs' })
})
api.get('/t7/b', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t7/b/index.ejs',
        { title: 'Lokeswari Pittu', layout: 'layout.ejs' })
})
api.get('/t7/c', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t7/c/index.ejs',
        { title: 'Sreelekha Vijaya', layout: 'layout.ejs' })
})


module.exports = api
