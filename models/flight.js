// developer -Sai Sirisha Devineni
const mongoose = require('mongoose')

const FlightSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  name: {
    type: String,
    required: true,
    default: 'Lexie'
  },
  startingPoint: {
    type: String,
    required: true,
    default: 'KEVU'
  },
  endingPoint: {
    type: String,
    required: true,
    default: 'KCPS'
  },
  durationHours: {
    type: Number,
    required: true,
    default: 1
  },
  pilotId:{
    type:Number,
    required:true,
  },
  planeId:{
    type:Number,
    required:true,
  }
})
module.exports = mongoose.model('Flight', FlightSchema)
